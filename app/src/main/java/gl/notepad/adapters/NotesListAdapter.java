package gl.notepad.adapters;

import java.util.List;

import gl.notepad.R;
import gl.notepad.notes.Note;
import android.content.Context;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NotesListAdapter extends ArrayAdapter<Note> {

	Context ctx;
	int res;
	List<Note> obj;
	String dateTimeFormat = "%Y-%m-%d %H:%M:%S";

	public NotesListAdapter(Context context, int resource, List<Note> objects) {
		super(context, resource, objects);
		ctx = context;
		res = resource;
		obj = objects;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View listItem = inflater.inflate(res, parent, false);
		TextView noteTitle = (TextView) listItem
				.findViewById(R.id.textView_noteTitle);
		TextView noteCreated = (TextView) listItem
				.findViewById(R.id.textView_noteCreated);
		Note n = obj.get(position);

		noteTitle.setText(n.getTitle());

		Time t = new Time();
		t.parse(n.getCreated().substring(0, 14));
		noteCreated.setText(t.format(dateTimeFormat));

		return listItem;
	}

	public void addAll(List<Note> collection) {
		for (Note n : collection) {
			this.add(n);
		}
	}

}
