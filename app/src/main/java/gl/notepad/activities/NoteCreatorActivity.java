package gl.notepad.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import gl.notepad.R;
import gl.notepad.fragments.BirthdayNoteCreatorFragment;
import gl.notepad.fragments.BirthdayNoteEditorFragment;
import gl.notepad.fragments.MeetingNoteCreatorFragment;
import gl.notepad.fragments.MeetingNoteEditorFragment;
import gl.notepad.fragments.ShoppingListCreatorFragment;
import gl.notepad.fragments.ShoppingListEditorFragment;
import gl.notepad.fragments.SimpleNoteCreatorFragment;
import gl.notepad.fragments.SimpleNoteEditorFragment;
import gl.notepad.notes.NoteType;

public class NoteCreatorActivity extends ActionBarActivity implements
        SimpleNoteCreatorFragment.OnFragmentInteractionListener,
        SimpleNoteEditorFragment.OnFragmentInteractionListener,
        BirthdayNoteCreatorFragment.OnFragmentInteractionListener,
        BirthdayNoteEditorFragment.OnFragmentInteractionListener,
        MeetingNoteCreatorFragment.OnFragmentInteractionListener,
        MeetingNoteEditorFragment.OnFragmentInteractionListener,
        ShoppingListCreatorFragment.OnFragmentInteractionListener,
        ShoppingListEditorFragment.OnFragmentInteractionListener {

    public static final String CREATE_NOTE_TYPE = "createType";
    public static final String TAG = "NoteCreatorActivity";
    Fragment fragment;
    String SAVED_INSTANCE_FRAGMENT = "saveInstanceFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_creator);

        if (savedInstanceState != null) {
            //TODO: odzyskanie poprzedniego stanu fragmentu
        } else {
            int i = getIntent().getIntExtra(CREATE_NOTE_TYPE, 0);
            switch (NoteType.values()[i]) {
                case SHOPPING_LIST:
                    fragment = new ShoppingListCreatorFragment();
                    break;
                case BIRTHDAY:
                    fragment = new BirthdayNoteCreatorFragment();
                    break;
                case MEETING:
                    fragment = new MeetingNoteCreatorFragment();
                    break;
                case SIMPLE:
                    fragment = new SimpleNoteCreatorFragment();
                    break;
            }
        }

        FragmentTransaction fTransaction = getSupportFragmentManager().beginTransaction();
        fTransaction.replace(R.id.note_create_fragment_container, fragment);
        fTransaction.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        if (fragment instanceof ShoppingListCreatorFragment) {
            MenuItem addItem = menu.add(Menu.NONE, R.id.action_add_item, Menu.NONE, R.string.action_add_item);
            MenuItemCompat.setShowAsAction(addItem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        }
        getMenuInflater().inflate(R.menu.note_creator, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_save:
                // TODO: obsłużenie zapisu notatki
                
                break;
            case R.id.action_add_item:
                // TODO: obsłużenie dodania produktu do listy
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
