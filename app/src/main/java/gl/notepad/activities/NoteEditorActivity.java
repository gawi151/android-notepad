package gl.notepad.activities;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import gl.notepad.R;
import gl.notepad.notes.Note;
import gl.notepad.notes.utils.NoteIO;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.text.format.Time;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class NoteEditorActivity extends ActionBarActivity {

    Note n;
    EditText title;
    EditText body;
    int mode;
    static final int EDIT_MODE = 1;
    static final int VIEW_MODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_editor);
        // Show the Up button in the action bar.
        setupActionBar();

        //title = (EditText) findViewById(R.id.editText_noteTitle);
        //body = (EditText) findViewById(R.id.editText_noteBody);

        setAndChangeMode(VIEW_MODE);

        if (getIntent().getExtras() != null) {
            n = (Note) getIntent().getSerializableExtra(Note.XML_TAG_NOTE);
            title.setText(n.getTitle());
            body.setText(n.getBody());
        } else {
            setAndChangeMode(EDIT_MODE);
            n = new Note();
        }
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (mode == EDIT_MODE) {
            getMenuInflater().inflate(R.menu.note_editor_editmode, menu);
        } else {
            getMenuInflater().inflate(R.menu.note_editor_viewmode, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_save:
                // zapisz dane w obiekcie
                // aktualizuj xml jeśli istnieje lub utwórz nowy xml

                if (title.length() != 0 && body.length() != 0) {
                    if (mode == EDIT_MODE) {
                        Time t = new Time();
                        t.setToNow();
                        n.setModified(t.format(Note.DATE_TIME_FORMAT));
                    }
                    n.setTitle(title.getText().toString());
                    n.setBody(body.getText().toString());

                    FileOutputStream fOut;
                    try {
                        fOut = openFileOutput("note" + n.getCreated(),
                                Context.MODE_PRIVATE);
                        OutputStreamWriter osw = new OutputStreamWriter(fOut);

                        // Write the string to the file
                        osw.write(NoteIO.writeToXML(n));
                    /*
                     * ensure that everything is really written out and close
					 */
                        osw.flush();
                        osw.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        finish();
                    }
                    return true;
                } else {
                    // info for user
                    Toast.makeText(this, "Wypełnij wszystkie pola",
                            Toast.LENGTH_SHORT).show();
                }
            case R.id.action_edit:
                if (mode != EDIT_MODE) {
                    title.setEnabled(true);
                    body.setEnabled(true);
                    mode = EDIT_MODE;
                } else {
                    title.setEnabled(false);
                    body.setEnabled(false);
                    mode = VIEW_MODE;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("mode", mode);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        setAndChangeMode(savedInstanceState.getInt("mode"));
        super.onRestoreInstanceState(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setAndChangeMode(int mode) {
        this.mode = mode;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            invalidateOptionsMenu();
        }
        switch (mode) {
            case VIEW_MODE:
                title.setEnabled(false);
                body.setEnabled(false);
                return;
            case EDIT_MODE:
                title.setEnabled(true);
                body.setEnabled(true);
                return;
        }
    }

}
