// TASK_DONE: Doda� menu kontekstowe dla android 4.0+
// http://developer.android.com/guide/topics/ui/menus.html#context-menu
/* TODO: Doda� ikony oraz zaj�� si� dopieszczaniem layout'u
 - podgl�d notatki (nie ma wygl�da� jak edycja, zmiana koloru itp)
 - 
 - doda� t�a dla notatek, mo�liwo�� zmian czcionki dla notatek
   przez u�ytkownika (aktywno�� Settings)
 */
// TODO: waznosc/kolory notatek (czerwona - wa�na, ��ta - �redniowa�na, zielona - info itp)
// TODO: ogarn�� zmian� orientacji (portrait/landscape) - zapisywanie informacji i odzyskanie

package gl.notepad.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import gl.notepad.R;
import gl.notepad.adapters.NotesListAdapter;
import gl.notepad.notes.Note;
import gl.notepad.notes.NoteType;
import gl.notepad.notes.utils.NoteIO;

public class MainActivity extends ActionBarActivity implements OnItemClickListener {

    ListView notesListView;
    NotesListAdapter notesAdapter;
    List<Note> notesList;
    public static Context AppContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Notes Main Activity", "onCreate");
        super.onCreate(savedInstanceState);
        AppContext = getApplicationContext();
        setContentView(R.layout.activity_main);
        loadNotes();

        notesListView = (ListView) findViewById(android.R.id.list);
        notesAdapter = new NotesListAdapter(this,
                R.layout.notes_list_item, notesList);
        notesListView.setAdapter(notesAdapter);
        notesListView.setOnItemClickListener(this);
        setUpContextualMenu();

    }

    @SuppressLint("NewApi")
    private void setUpContextualMenu() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            MultiChoiceModeListener multiChoiceModeListener = new MultiChoiceModeListener() {
                ArrayList<Integer> checkedPositions = new ArrayList<Integer>();

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    Log.i("checkedPositions - onDestroyActionMode",
                            checkedPositions.toString());
                    if (checkedPositions.isEmpty()) {
                        return;
                    }
                    checkedPositions.clear();
                }

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    mode.setTag("Options");
                    mode.getMenuInflater().inflate(R.menu.context_menu, menu);
                    return true;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode,
                                                   MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_delete:
                            for (int i : checkedPositions) {
                                deleteNote(i);
                            }
                            mode.finish();
                            return true;
                        default:
                            return false;
                    }
                }

                @Override
                public void onItemCheckedStateChanged(ActionMode mode,
                                                      int position, long id, boolean checked) {
                    View v = notesListView.getChildAt(position
                            - notesListView.getFirstVisiblePosition());
                    if (v == null) {
                        return;
                    }
                    if (checked) {
                        checkedPositions.add(position);

                    } else {
                        checkedPositions.remove(checkedPositions
                                .lastIndexOf(position));
                    }
                }
            };
            notesListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            notesListView.setMultiChoiceModeListener(multiChoiceModeListener);
        } else {
            registerForContextMenu(notesListView);
        }
    }

    private void loadNotes() {
        String[] notesFileNames = getApplicationContext().fileList();
        notesList = new ArrayList<Note>();
        Note n = new Note();
        for (String s : notesFileNames) {
            try {
                if (s.startsWith("note")) {
                    notesList.add(NoteIO.readFromXML(openFileInput(s)));
//                    switch (n.checkType(openFileInput(s))){
//                        case SIMPLE:
//                            n = new Note();
//                            notesList.add(n.readFromXml(openFileInput(s)));
//                            break;
//                        case MEETING:
//                            n = new Meeting();
//                            notesList.add(n.readFromXml(openFileInput(s)));
//                            break;
//                        case BIRTHDAY:
//                            n = new Birthday();
//                            notesList.add(n.readFromXml(openFileInput(s)));
//                            break;
//                        case SHOPPING_LIST:
//                            n = new ShoppingList();
//                            notesList.add(n.readFromXml(openFileInput(s)));
//                            break;
//                    }
                }
            } catch (FileNotFoundException e) {
                continue;
            }
        }
    }

    private void deleteNote(int position) {
        Note tempNote = (Note) notesListView.getItemAtPosition(position);
        if (deleteFile("note" + tempNote.getCreated())) {
            notesAdapter.remove(tempNote);
            notesAdapter.notifyDataSetChanged();
            tempNote = null;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) item
                .getMenuInfo();
        switch (item.getItemId()) {
            case R.id.action_delete:
                deleteNote(menuInfo.position);
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        Log.i("Notes Main Activity", "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.i("Notes Main Activity", "onPause");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        Log.i("Notes Main Activity", "onRestart");
        super.onRestart();
        loadNotes();
        notesAdapter.clear();
        notesAdapter.addAll(notesList);
        notesAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        Log.i("Notes Main Activity", "onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        Log.i("Notes Main Activity", "onStop");
        super.onStop();
    }

    @Override
    protected void onStart() {
        Log.i("Notes Main Activity", "onStart");
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("Notes Main Activity", "onActivityResult");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.i("Notes Main Activity", "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        Log.i("Notes Main Activity", "onRestoreInstanceState");
        super.onRestoreInstanceState(state);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:
                DialogFragment dialog = new ChooseNoteDialogFragment();
                dialog.show(getSupportFragmentManager(), "chooseNote");
            /*
            Intent i = new Intent(this, NoteEditActivity.class);
			startActivity(i);
			*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Intent i = new Intent(this, NoteEditorActivity.class);
        i.putExtra(Note.XML_TAG_NOTE, (Note) parent.getItemAtPosition(position));
        startActivityForResult(i, 0);
    }

    public static class ChooseNoteDialogFragment extends DialogFragment {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            dialogBuilder.setTitle(R.string.choose_note)
                    .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .setItems(NoteType.getTypes(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // TODO: dodać przejście do aktywności utworzenia odpowiedniego typu notatki
                            Intent intent = new Intent(getActivity().getBaseContext(), NoteCreatorActivity.class);
                            intent.putExtra(NoteCreatorActivity.CREATE_NOTE_TYPE, i);
                            startActivity(intent);
                        }
                    });
            return dialogBuilder.create();
        }
    }

}
