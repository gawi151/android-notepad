package gl.notepad.fragments;

import android.support.v4.app.Fragment;

import gl.notepad.notes.Note;

public abstract class NoteEditorFragment extends Fragment {

    public NoteEditorFragment() {
        // Required empty public constructor
    }

    protected abstract Note onLoadNote();

}
