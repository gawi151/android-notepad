package gl.notepad.fragments;

import android.support.v4.app.Fragment;

public abstract class NoteCreatorFragment extends Fragment {

    public NoteCreatorFragment() {
        // Required empty public constructor
    }

    protected abstract void onSaveNote();
}
