package gl.notepad.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import gl.notepad.R;
import gl.notepad.notes.Note;
import gl.notepad.notes.utils.NoteIO;

public class SimpleNoteCreatorFragment extends NoteCreatorFragment {

    EditText title;
    EditText body;
    Note note;

    private OnFragmentInteractionListener mListener;

    public SimpleNoteCreatorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_simple_note_creator, container, false);
        title = (EditText) v.findViewById(R.id.fragment_simple_note_creator_title);
        body = (EditText) v.findViewById(R.id.fragment_simple_note_creator_body);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void onSaveNote() {
        note = new Note(title.getText().toString(), body.getText().toString());
        if (title.length() != 0 && body.length() != 0) {
            FileOutputStream fOut;
            try {
                fOut = getActivity().openFileOutput("note" + note.getCreated(),
                        Context.MODE_PRIVATE);
                OutputStreamWriter osw = new OutputStreamWriter(fOut);

                // Write the string to the file
                osw.write(NoteIO.writeToXML(note));
                    /*
                     * ensure that everything is really written out and close
					 */
                osw.flush();
                osw.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                getActivity().finish();
            }
        } else {
            // info for user
            Toast.makeText(getActivity().getBaseContext(), R.string.toast_empty_field,
                    Toast.LENGTH_SHORT).show();
        }
        NoteIO.writeToXML(note);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
