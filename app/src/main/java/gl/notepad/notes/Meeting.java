package gl.notepad.notes;

/**
 * Created by Łukasz Gawron on 2014-08-21.
 */

public class Meeting extends Note{

    /*
     * Fields
     */
    private String meetingTime;
    private String meetingDate;
    private Reminder reminder;

    public static final String XML_TYPE = NoteType.MEETING.name();
    public static final String XML_TAG_MEETING = "meeting";
    public static final String XML_TAG_REMINDER = "reminder";
    public static final String XML_TAG_DATE = "date";
    public static final String XML_TAG_TIME = "time";
    public static final String XML_ATTRIBUTE_REMIND = "remind";

    /*
     * Constructors
     */

    public Meeting() {
        this("", "");
    }

    public Meeting(String title, String text) {
        this(title, text, false);
    }

    public Meeting(String title, String text, boolean remind) {
        super(title, text);
        this.type = NoteType.MEETING;
        this.reminder = new Reminder(remind);
    }

    /*
     * Setters and Getters
     */

    public String getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }

    public String getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(String meetingDate) {
        this.meetingDate = meetingDate;
    }

    public Reminder getReminder() {
        return reminder;
    }

    public void setReminder(Reminder reminder) {
        this.reminder = reminder;
    }

    /*
     * Methods
     */

    public class Reminder {

        private boolean remind;

        public Reminder(boolean remind){
            this.setRemind(remind);
        }

        public boolean isRemind() {
            return remind;
        }

        public void setRemind(boolean remind) {
            this.remind = remind;
        }
    }

}
