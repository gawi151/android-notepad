package gl.notepad.notes;

/**
 * Created by Łukasz Gawron on 2014-08-21.
 */
public class Birthday extends Meeting{

    /*
     * Fields
     */

    private String celebratingPerson;

    public static final String XML_TYPE = NoteType.BIRTHDAY.name();
    public static final String XML_TAG_WHO_CELEBRATE = "whoCelebrate";

    /*
     * Constructors
     */

    public Birthday(){
        this("", "");
    }

    public Birthday(String title, String text){
        super(title, text);
        this.type = NoteType.BIRTHDAY;
    }

    /*
     * Setters and getters.
     */

    public String getCelebratingPerson() {
        return celebratingPerson;
    }

    public void setCelebratingPerson(String celebratingPerson) {
        this.celebratingPerson = celebratingPerson;
    }

    /*
     *  Methods/Functions
     */

}
