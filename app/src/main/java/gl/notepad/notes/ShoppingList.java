package gl.notepad.notes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Łukasz Gawron on 2014-08-21.
 */
public class ShoppingList extends Note{

    private List<ShoppingItem> list;

    public static final String XML_TYPE = NoteType.SHOPPING_LIST.name();
    public static final String XML_TAG_LIST = "list";
    public static final String XML_TAG_ITEM = "item";
    public static final String XML_ATTRIBUTE_CHECKED = "checked";

    public ShoppingList(){
        this("");
    }

    public ShoppingList(String title){
        super(title, "");
        this.list = new ArrayList<ShoppingItem>();
        this.type = NoteType.SHOPPING_LIST;
    }

    public ShoppingList(String title, String[] items){
        this(title);
        this.list = new ArrayList<ShoppingItem>();
        addItems(items);
    }

    public List<ShoppingItem> getList() {
        return list;
    }

    public void setList(List<ShoppingItem> list) {
        this.list = list;
    }

    public void addItem(String name) {
        list.add(new ShoppingItem(name));
    }

    public void addItem(String name, boolean isChecked) {
        list.add(new ShoppingItem(name, isChecked));
    }

    public void addItems(String[] names) {
        List<ShoppingItem> tempList = new ArrayList<ShoppingItem>();
        for (String s : names) {
            tempList.add(new ShoppingItem(s));
        }
        list.addAll(tempList);
    }

    public boolean deleteItem(String name) {
        ShoppingItem item = new ShoppingItem(name);
        return list.remove(item);
    }

    public boolean deleteItems(String[] names) {
        List<ShoppingItem> tempList = new ArrayList<ShoppingItem>();
        for (String s : names) {
            tempList.add(new ShoppingItem(s));
        }
        return list.removeAll(tempList);
    }

    public class ShoppingItem {

        private String name;
        private boolean checked;

        public ShoppingItem(String name) {
            setName(name);
            setChecked(false);
        }

        public ShoppingItem(String name, boolean isChecked) {
            setName(name);
            setChecked(isChecked);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

}
