package gl.notepad.notes;

/**
 * Created by Łukasz Gawron.
 */

import android.text.format.Time;

import java.io.Serializable;

/**
 * Basic class representing application notes.
 */
public class Note implements Serializable {

    /*
     * Fields
     */

    // Inherited fields
    protected String title;
    protected String body;
    protected String created;
    protected String modified;
    protected NoteType type;

    // Class specific constants (private)

    // Public constants
    public static final String XML_TYPE = NoteType.SIMPLE.name();
    public static final String DATE_TIME_FORMAT = "%Y%m%dT%H%M%S%z";
    public static final String XML_ENCODE = "UTF-8";
    public static final String XML_TAG_NOTE = "note";
    public static final String XML_ATTRIBUTE_CREATED = "created";
    public static final String XML_ATTRIBUTE_MODIFIED = "modified";
    public static final String XML_ATTRIBUTE_TYPE = "type";
    public static final String XML_TAG_TITLE = "title";
    public static final String XML_TAG_BODY = "body";

   /*
    * Constructors
    */

    /**
     * Constructor with specified title and text of note
     *
     * @param title - String variable with title of note
     * @param text  - String variable with text of note
     */
    public Note(String title, String text) {
        this();
        this.title = title;
        this.body = text;
    }

    /**
     * Default constructor
     * Sets empty title and body text
     */
    public Note() {
        this.title = "";
        this.body = "";
        this.type = NoteType.SIMPLE;
        Time t = new Time();
        t.setToNow();
        this.created = t.format(DATE_TIME_FORMAT);
        this.modified = t.format(DATE_TIME_FORMAT);
    }

   /*
    * Setters and Getters
    */

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public NoteType getType() {
        return type;
    }

    /*
     * Methods
     */

    @Override
    public String toString() {
        return title;
    }


}
