package gl.notepad.notes.utils;

/**
 * Created by Łukasz Gawron.
 */

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import gl.notepad.notes.Birthday;
import gl.notepad.notes.Meeting;
import gl.notepad.notes.Note;
import gl.notepad.notes.NoteType;
import gl.notepad.notes.ShoppingList;

/*
 * Notes xml file representations, specified by type.
 *
 * <note created="" modified="" type="simple">
 *  <title>...</title>
 *  <body>...</body>
 * </note>
 *
 * <note created="" modified="" type="meeting">
 *  <title>...</title>
 *  <body>...</body>
 *  <meeting remind="true|false">
 *      <date>...</date>
 *      <time>...</time>
 *      <reminder>...</reminder>
 *  </meeting>
 * </note>
 *
 * <note created="" modified="" type="birthday">
 *  <title>...</title>
 *  <body>...</body>
 *  <whoCelebrate>...</whoCelebrate>
 *  <meeting remind="true|false">
 *      <date>...</date>
 *      <time>...</time>
 *      <reminder>...</reminder>
 *  </meeting>
 * </note>
 *
 * <note created="" modified="" type="shopping">
 *  <title>...</title>
 *  <body>...</body>
 *  <list>
 *      <item checked="[true|false]">...</item>
 *      <item>...</item>
 *      <item>...</item>
 *      <item>...</item>
 *  </list>
 * </note>
 */


public final class NoteIO {

    public NoteIO() {
    }

    public static Note readFromXML(InputStream xml) {
        switch (checkNoteTypeFromXml(xml)) {
            case SIMPLE:
                return readSimpleNoteFromXML(xml);
            case MEETING:
                return readMeetingNoteFromXML(xml);
            case BIRTHDAY:
                return readBirthdayNoteFromXML(xml);
            case SHOPPING_LIST:
                return readShoppingNoteFromXML(xml);
            default:
                throw new IllegalArgumentException("Unknown note type.");
        }
    }

    public static String writeToXML(Note note) {
        switch (note.getType()) {
            case SHOPPING_LIST:
                return writeShoppingNoteToXML(note);
            case BIRTHDAY:
                return writeBirthdayNoteToXML(note);
            case MEETING:
                return writeMeetingNoteToXML(note);
            case SIMPLE:
                return writeSimpleNoteToXML(note);
            default:
                throw new IllegalArgumentException("Unknown note type.");
        }
    }

/* <note created="" modified="" type="meeting">
 *  <title>...</title>
 *  <body>...</body>
 *  <meeting remind="true|false">
 *      <date>...</date>
 *      <time>...</time>
 *      <reminder>...</reminder>
 *  </meeting>
 * </note>
 */

    private static String writeMeetingNoteToXML(Note note) {
        XmlSerializer s = Xml.newSerializer();
        StringWriter w = new StringWriter();
        Meeting m = (Meeting) note;
        try {
            s.setOutput(w);
            s.startDocument(Note.XML_ENCODE, true);
            s.text("\n");
            s.startTag("", Note.XML_TAG_NOTE);
            {
                s.attribute("", Note.XML_ATTRIBUTE_CREATED, m.getCreated());
                s.attribute("", Note.XML_ATTRIBUTE_MODIFIED, m.getModified());
                s.attribute("", Note.XML_ATTRIBUTE_TYPE, m.getType().name());
                s.text("\n\t");
                s.startTag("", Note.XML_TAG_TITLE);
                {
                    s.text(m.getTitle());
                }
                s.endTag("", Note.XML_TAG_TITLE);
                s.text("\n\t");
                s.startTag("", Note.XML_TAG_BODY);
                {
                    s.text(m.getBody());
                }
                s.endTag("", Note.XML_TAG_BODY);
                s.text("\n\t");
                s.startTag("", Meeting.XML_TAG_MEETING);
                {
                    s.attribute("", Meeting.XML_ATTRIBUTE_REMIND,
                            String.valueOf(m.getReminder().isRemind()));
                    s.text("\n\t\t");
                    s.startTag("", Meeting.XML_TAG_DATE);
                    {
                        s.text(m.getMeetingDate());
                    }
                    s.endTag("", Meeting.XML_TAG_DATE);
                    s.text("\n\t\t");
                    s.startTag("", Meeting.XML_TAG_TIME);
                    {
                        s.text(m.getMeetingTime());
                    }
                    s.endTag("", Meeting.XML_TAG_TIME);
                    if (m.getReminder().isRemind()) {
                        // TODO: przyszła funkcjonalność przypominania o spotkaniu
                        s.text("\n\t\t");
                        s.startTag("", Meeting.XML_TAG_REMINDER);
                        {

                        }
                        s.endTag("", Meeting.XML_TAG_REMINDER);
                    }
                }
                s.text("\n\t");
                s.endTag("", Meeting.XML_TAG_MEETING);
            }
            s.text("\n");
            s.endTag("", Note.XML_TAG_NOTE);
            s.endDocument();
            return w.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Note readMeetingNoteFromXML(InputStream xml) {
        Meeting n = null;
        String text = "";
        XmlPullParser p = Xml.newPullParser();
        try {
            p.setInput(xml, Note.XML_ENCODE);
            int eventType = p.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = p.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagName.equalsIgnoreCase(Note.XML_TAG_NOTE)) {
                            // create a new instance
                            n = new Meeting();
                            n.setCreated(p.getAttributeValue("", Note.XML_ATTRIBUTE_CREATED));
                            n.setModified(p.getAttributeValue("", Note.XML_ATTRIBUTE_MODIFIED));
                        } else if (tagName.equalsIgnoreCase(Meeting.XML_TAG_MEETING)) {
                            n.getReminder()
                                    .setRemind(Boolean.parseBoolean(
                                                    p.getAttributeValue("", Meeting.XML_ATTRIBUTE_REMIND))
                                    );
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = p.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagName.equalsIgnoreCase(Note.XML_TAG_NOTE)) {
                            return n;
                        } else if (tagName.equalsIgnoreCase(Note.XML_TAG_TITLE)) {
                            n.setTitle(text);
                        } else if (tagName.equalsIgnoreCase(Note.XML_TAG_BODY)) {
                            n.setBody(text);
                        } else if (tagName.equalsIgnoreCase(Meeting.XML_TAG_DATE)) {
                            n.setMeetingDate(text);
                        } else if (tagName.equalsIgnoreCase(Meeting.XML_TAG_TIME)) {
                            n.setMeetingTime(text);
                        } else if (tagName.equalsIgnoreCase(Meeting.XML_TAG_REMINDER) &&
                                n.getReminder().isRemind()) {
                            // TODO: obsluga wczytywania przypominajki
                        }
                        break;

                    default:
                        break;
                }
                eventType = p.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

   /* <note created="" modified="" type="birthday">
    *  <title>...</title>
    *  <body>...</body>
    *  <whoCelebrate>...</whoCelebrate>
    *  <meeting remind="true|false">
    *      <date>...</date>
    *      <time>...</time>
    *      <reminder>...</reminder>
    *  </meeting>
    * </note>
    */

    private static String writeBirthdayNoteToXML(Note note) {
        XmlSerializer s = Xml.newSerializer();
        StringWriter w = new StringWriter();
        Birthday b = (Birthday) note;
        try {
            s.setOutput(w);
            s.startDocument(Note.XML_ENCODE, true);
            s.text("\n");
            s.startTag("", Note.XML_TAG_NOTE);
            {
                s.attribute("", Note.XML_ATTRIBUTE_CREATED, b.getCreated());
                s.attribute("", Note.XML_ATTRIBUTE_MODIFIED, b.getModified());
                s.attribute("", Note.XML_ATTRIBUTE_TYPE, b.getType().name());
                s.text("\n\t");
                s.startTag("", Note.XML_TAG_TITLE);
                {
                    s.text(b.getTitle());
                }
                s.endTag("", Note.XML_TAG_TITLE);
                s.text("\n\t");
                s.startTag("", Note.XML_TAG_BODY);
                {
                    s.text(b.getBody());
                }
                s.endTag("", Note.XML_TAG_BODY);
                s.text("\n");
                s.startTag("", Birthday.XML_TAG_WHO_CELEBRATE);
                {
                    s.text(b.getCelebratingPerson());
                }
                s.endTag("", Birthday.XML_TAG_WHO_CELEBRATE);
                s.text("\n");
                s.startTag("", Meeting.XML_TAG_MEETING);
                {
                    s.attribute("", Meeting.XML_ATTRIBUTE_REMIND,
                            String.valueOf(b.getReminder().isRemind()));
                    s.text("\n\t\t");
                    s.startTag("", Meeting.XML_TAG_DATE);
                    {
                        s.text(b.getMeetingDate());
                    }
                    s.endTag("", Meeting.XML_TAG_DATE);
                    s.text("\n\t\t");
                    s.startTag("", Meeting.XML_TAG_TIME);
                    {
                        s.text(b.getMeetingTime());
                    }
                    s.endTag("", Meeting.XML_TAG_TIME);
                    if (b.getReminder().isRemind()) {
                        // TODO: przyszła funkcjonalność przypominania o spotkaniu
                        s.text("\n\t\t");
                        s.startTag("", Meeting.XML_TAG_REMINDER);
                        {

                        }
                        s.endTag("", Meeting.XML_TAG_REMINDER);
                    }
                }
                s.text("\n\t");
                s.endTag("", Meeting.XML_TAG_MEETING);
                s.endTag("", Note.XML_TAG_NOTE);
            }
        } catch (Exception e) {

        }
        return w.toString();
    }

    private static Birthday readBirthdayNoteFromXML(InputStream xml) {
        Birthday b = null;
        String text = "";
        XmlPullParser p = Xml.newPullParser();
        try {
            p.setInput(xml, Note.XML_ENCODE);
            int eventType = p.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = p.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagName.equalsIgnoreCase(Note.XML_TAG_NOTE)) {
                            // create a new instance
                            b = new Birthday();
                            b.setCreated(p.getAttributeValue("", Note.XML_ATTRIBUTE_CREATED));
                            b.setModified(p.getAttributeValue("", Note.XML_ATTRIBUTE_MODIFIED));
                        } else if (tagName.equalsIgnoreCase(Meeting.XML_TAG_MEETING)) {
                            b.getReminder()
                                    .setRemind(Boolean.parseBoolean(
                                                    p.getAttributeValue("", Meeting.XML_ATTRIBUTE_REMIND))
                                    );
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = p.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagName.equalsIgnoreCase(Note.XML_TAG_NOTE)) {
                            return b;
                        } else if (tagName.equalsIgnoreCase(Note.XML_TAG_TITLE)) {
                            b.setTitle(text);
                        } else if (tagName.equalsIgnoreCase(Note.XML_TAG_BODY)) {
                            b.setBody(text);
                        } else if (tagName.equalsIgnoreCase(Meeting.XML_TAG_DATE)) {
                            b.setMeetingDate(text);
                        } else if (tagName.equalsIgnoreCase(Meeting.XML_TAG_TIME)) {
                            b.setMeetingTime(text);
                        } else if (tagName.equalsIgnoreCase(Meeting.XML_TAG_REMINDER) &&
                                b.getReminder().isRemind()) {
                            // TODO: obsluga wczytywania przypominajki
                        } else if (tagName.equalsIgnoreCase(Birthday.XML_TAG_WHO_CELEBRATE)) {
                            b.setCelebratingPerson(text);
                        }
                        break;

                    default:
                        break;
                }
                eventType = p.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /* <note created="" modified="" type="shopping">
     *  <title>...</title>
     *  <body>...</body>
     *  <list>
     *      <item checked="[true|false]">...</item>
     *      <item>...</item>
     *      <item>...</item>
     *      <item>...</item>
     *  </list>
     * </note>
     */

    private static String writeShoppingNoteToXML(Note note) {
        XmlSerializer s = Xml.newSerializer();
        StringWriter w = new StringWriter();
        ShoppingList sl = (ShoppingList) note;
        try {
            s.setOutput(w);
            s.startDocument(Note.XML_ENCODE, true);
            s.text("\n");
            s.startTag("", Note.XML_TAG_NOTE);
            s.attribute("", Note.XML_ATTRIBUTE_CREATED, sl.getCreated());
            s.attribute("", Note.XML_ATTRIBUTE_MODIFIED, sl.getModified());
            s.attribute("", Note.XML_ATTRIBUTE_TYPE, sl.getType().name());
            s.text("\n\t");
            s.startTag("", Note.XML_TAG_TITLE);
            s.text(sl.getTitle());
            s.endTag("", Note.XML_TAG_TITLE);
            s.text("\n\t");
            s.startTag("", Note.XML_TAG_BODY);
            s.text(sl.getBody());
            s.endTag("", Note.XML_TAG_BODY);
            s.text("\n\t");
            s.startTag("", ShoppingList.XML_TAG_LIST);
            {
                for (ShoppingList.ShoppingItem item : sl.getList()) {
                    s.text("\n\t\t");
                    s.startTag("", ShoppingList.XML_TAG_ITEM);
                    s.attribute("", ShoppingList.XML_ATTRIBUTE_CHECKED, String.valueOf(item.isChecked()));
                    s.text(item.getName());
                    s.endTag("", ShoppingList.XML_TAG_ITEM);
                }
            }
            s.text("\n\t");
            s.endTag("", ShoppingList.XML_TAG_LIST);
            s.text("\n");
            s.endTag("", Note.XML_TAG_NOTE);
            s.endDocument();
            return w.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static ShoppingList readShoppingNoteFromXML(InputStream xml) {
        ShoppingList sl = null;
        boolean checked = false;
        String text = "";
        XmlPullParser p = Xml.newPullParser();
        try {
            p.setInput(xml, Note.XML_ENCODE);
            int eventType = p.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = p.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagName.equalsIgnoreCase(Note.XML_TAG_NOTE)) {
                            // create a new instance
                            sl = new ShoppingList();
                            sl.setCreated(p.getAttributeValue("", Note.XML_ATTRIBUTE_CREATED));
                            sl.setModified(p.getAttributeValue("", Note.XML_ATTRIBUTE_MODIFIED));
                        } else if (tagName.equalsIgnoreCase(ShoppingList.XML_TAG_ITEM)) {
                            checked = Boolean.parseBoolean(p.getAttributeValue("", ShoppingList.XML_ATTRIBUTE_CHECKED));
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = p.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagName.equalsIgnoreCase(Note.XML_TAG_NOTE)) {
                            return sl;
                        } else if (tagName.equalsIgnoreCase(Note.XML_TAG_TITLE)) {
                            sl.setTitle(text);
                        } else if (tagName.equalsIgnoreCase(Note.XML_TAG_BODY)) {
                            sl.setBody(text);
                        } else if (tagName.equalsIgnoreCase(ShoppingList.XML_TAG_ITEM)) {
                            sl.addItem(text, checked);
                        }
                        break;

                    default:
                        break;
                }
                eventType = p.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

   /* <note created="" modified="" type="simple">
    *  <title>...</title>
    *  <body>...</body>
    * </note>
    */

    private static String writeSimpleNoteToXML(Note note) {
        XmlSerializer s = Xml.newSerializer();
        StringWriter w = new StringWriter();
        try {
            s.setOutput(w);
            s.startDocument(Note.XML_ENCODE, true);
            s.text("\n");
            s.startTag("", Note.XML_TAG_NOTE);
            s.attribute("", Note.XML_ATTRIBUTE_CREATED, note.getCreated());
            s.attribute("", Note.XML_ATTRIBUTE_MODIFIED, note.getModified());
            s.attribute("", Note.XML_ATTRIBUTE_TYPE, note.getType().name());
            s.text("\n\t");
            s.startTag("", Note.XML_TAG_TITLE);
            s.text(note.getTitle());
            s.endTag("", Note.XML_TAG_TITLE);
            s.text("\n\t");
            s.startTag("", Note.XML_TAG_BODY);
            s.text(note.getBody());
            s.endTag("", Note.XML_TAG_BODY);
            s.text("\n");
            s.endTag("", Note.XML_TAG_NOTE);
            s.endDocument();
            return w.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Note readSimpleNoteFromXML(InputStream xml) {
        Note n = null;
        String text = "";
        XmlPullParser p = Xml.newPullParser();
        try {
            p.setInput(xml, Note.XML_ENCODE);
            int eventType = p.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = p.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagName.equalsIgnoreCase(Note.XML_TAG_NOTE)) {
                            // create a new instance
                            n = new Note();
                            n.setCreated(p.getAttributeValue("", Note.XML_ATTRIBUTE_CREATED));
                            n.setModified(p.getAttributeValue("", Note.XML_ATTRIBUTE_MODIFIED));
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = p.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagName.equalsIgnoreCase(Note.XML_TAG_NOTE)) {
                            return n;
                        } else if (tagName.equalsIgnoreCase(Note.XML_TAG_TITLE)) {
                            n.setTitle(text);
                        } else if (tagName.equalsIgnoreCase(Note.XML_TAG_BODY)) {
                            n.setBody(text);
                        }
                        break;

                    default:
                        break;
                }
                eventType = p.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static NoteType checkNoteTypeFromXml(InputStream in) {
        XmlPullParser parser = Xml.newPullParser();
        String type = "";
        try {
            parser.setInput(in, Note.XML_ENCODE);
            int eventType = parser.getEventType();
            while ((eventType != XmlPullParser.START_TAG) && (parser.getName() != Note.XML_TAG_NOTE)) {
                eventType = parser.next();
            }
            type = parser.getAttributeValue("", Note.XML_ATTRIBUTE_TYPE);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return NoteType.valueOf(type);
    }

}
