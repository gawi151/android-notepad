package gl.notepad.notes;

import android.content.res.Resources;

import gl.notepad.R;
import gl.notepad.activities.MainActivity;

/**
 * Created by Łukasz Gawron on 2014-09-16.
 */
public enum NoteType {

    SIMPLE(R.string.note_simple),
    SHOPPING_LIST(R.string.note_shopping_list),
    MEETING(R.string.note_meeting),
    BIRTHDAY(R.string.note_birthday);

    private String type;
    private Resources res = MainActivity.AppContext.getResources();

    NoteType(int resourceId) {
        type = res.getString(resourceId);
    }

    @Override
    public String toString() {
        return type;
    }

    public static String[] getTypes() {
        String[] types = new String[NoteType.values().length];
        for (int i = 0; i < types.length; i++){
            types[i] = NoteType.values()[i].toString();
        }
        return types;
    }

}
