package gl.notepad.notes;

import java.io.InputStream;

/**
 * Created by Łukasz Gawron on 2014-09-16.
 */
public interface INoteXmlIO {

    public Note readFromXml(InputStream xmlStream);
    public String writeToXml();
    public NoteType checkType(InputStream xmlStream);

}
